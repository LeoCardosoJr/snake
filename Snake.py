import pygame
import sys
import time
import random

class Snake:
    def __init__(self):

        # Pygame Init
        init_status = pygame.init()
        if init_status[1] > 0:
            print("(!) Had {0} initialising errors, exiting... ".format(init_status[1]))
            sys.exit()
        else:
            print("(+) Pygame initialised successfully ")
            
        # Play Surface
        size = self.width, self.height = 640, 320
        pygame.display.set_caption("Snake Game")
        
        self.playSurface = pygame.display.set_mode(size)

        # Colors
        self.colorGameOver = pygame.Color(255, 0, 0)
        self.colorSnake = pygame.Color(0, 255, 0)
        self.colorScore = pygame.Color(0, 0, 0)
        self.colorSurface = pygame.Color(255, 255, 255)
        self.colorFood = pygame.Color(165, 42, 42)

        # FPS controller
        self.fpsController = pygame.time.Clock()

        # Game settings
        self.positionStep = 10
        self.snakePosition = [100, 50]
        self.snakeBody = [[100, 50], [90, 50], [80, 50]]
        self.foodPos = [400, 50]
        self.foodSpawn = True
        self.direction = 'RIGHT'
        self.changeto = ''
        self.score = 0

        self.Subscribers = []
        self.SubscribersEventQueue = []

    def reset(self):
        # FPS controller
        self.fpsController = pygame.time.Clock()
        self.positionStep = 10
        self.snakePosition = [100, 50]
        self.snakeBody = [[100, 50], [90, 50], [80, 50]]
        self.foodPos = [400, 50]
        self.foodSpawn = True
        self.direction = 'RIGHT'
        self.changeto = ''
        self.score = 0
        self.start()


    # Game Over
    def gameOver(self):
        myFont = pygame.font.SysFont('monaco', 72)
        GOsurf = myFont.render("Game Over", True, self.colorGameOver)
        GOrect = GOsurf.get_rect()
        GOrect.midtop = (320, 25)
        self.playSurface.blit(GOsurf, GOrect)
        self.showScore(0)
        pygame.display.flip()
        time.sleep(4)
        self.reset()
        # pygame.quit()
        # sys.exit()


    # Show Score
    def showScore(self, choice=1):
        SFont = pygame.font.SysFont('monaco', 32)
        Ssurf = SFont.render("Score  :  {0}".format(self.score), True, self.colorScore)
        Srect = Ssurf.get_rect()
        if choice == 1:
            Srect.midtop = (80, 10)
        else:
            Srect.midtop = (320, 100)
        self.playSurface.blit(Ssurf, Srect)

    def callSubscriber(self):
        self.SubscribersEventQueue = []
        if self.Subscribers:
            for i in self.Subscribers:
                event = i.snakeHandler()
                if (event):
                    self.SubscribersEventQueue.append(event)

    def setSubscriber(self, objeto):
        self.Subscribers.append(objeto)

    def getScore(self):
        return self.score

    def start(self):
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_RIGHT or event.key == pygame.K_d:
                        self.changeto = 'RIGHT'
                    if event.key == pygame.K_LEFT or event.key == pygame.K_a:
                        self.changeto = 'LEFT'
                    if event.key == pygame.K_UP or event.key == pygame.K_w:
                        self.changeto = 'UP'
                    if event.key == pygame.K_DOWN or event.key == pygame.K_s:
                        self.changeto = 'DOWN'
                    if event.key == pygame.K_ESCAPE:
                        pygame.event.post(pygame.event.Event(pygame.QUIT))

            self.score = self.score - 1 

            self.callSubscriber()
            #AI user events
            if self.SubscribersEventQueue:
                for event in self.SubscribersEventQueue:
                    if event== 'RIGHT':
                        self.changeto = 'RIGHT'
                    if event== 'LEFT':
                        self.changeto = 'LEFT'
                    if event== 'UP':
                        self.changeto = 'UP'
                    if event== 'DOWN':
                        self.changeto = 'DOWN'
                    if event== 'SPACE':
                        pygame.event.post(pygame.event.Event(pygame.QUIT))

            # Validate self.direction
            if self.changeto == 'RIGHT' and self.direction != 'LEFT':
                self.direction = self.changeto
            if self.changeto == 'LEFT' and self.direction != 'RIGHT':
                self.direction = self.changeto
            if self.changeto == 'UP' and self.direction != 'DOWN':
                self.direction = self.changeto
            if self.changeto == 'DOWN' and self.direction != 'UP':
                self.direction = self.changeto

            # Update snake position
            if self.direction == 'RIGHT':
                self.snakePosition[0] += self.positionStep
            if self.direction == 'LEFT':
                self.snakePosition[0] -= self.positionStep
            if self.direction == 'DOWN':
                self.snakePosition[1] += self.positionStep
            if self.direction == 'UP':
                self.snakePosition[1] -= self.positionStep

            # Snake body mechanism
            self.snakeBody.insert(0, list(self.snakePosition))
            if self.snakePosition == self.foodPos:
                self.foodSpawn = False
                self.score += 100
            else:
                self.snakeBody.pop()
            if self.foodSpawn == False:
                self.foodPos = [random.randrange(1, self.width // 10) * self.positionStep, random.randrange(1, self.height // 10) * self.positionStep]
                self.foodSpawn = True

            self.playSurface.fill(self.colorSurface)
            for pos in self.snakeBody:
                pygame.draw.rect(self.playSurface, self.colorSnake, pygame.Rect(pos[0], pos[1], self.positionStep, self.positionStep))
            pygame.draw.rect(self.playSurface, self.colorFood, pygame.Rect(self.foodPos[0], self.foodPos[1], self.positionStep, self.positionStep))

            # Bounds
            if self.snakePosition[0] >= self.width or self.snakePosition[0] < 0:
                self.gameOver()
            if self.snakePosition[1] >= self.height or self.snakePosition[1] < 0:
                self.gameOver()

            # Self hit
            for block in self.snakeBody[1:]:
                if self.snakePosition == block:
                    self.gameOver()
            self.showScore()
            pygame.display.flip()

            self.fpsController.tick(20)