#   SNAKE GAME
#   Author : Não é mais ele
#   Python 3.5.2 Pygame

from Snake import Snake

class Snake_Player:
    def __init__(self, game):
        if (not game):
            self.game = Snake()

        self.game = game
        self.stepCounter = 0
        game.setSubscriber(self)

    def funcaoAvaliacao(self):
        pass

    def snakeHandler(self):
        return 'NONE'
        self.stepCounter = self.stepCounter + 1
        if self.stepCounter == 10:
            return 'DOWN'
        elif self.stepCounter == 20:
            return 'LEFT'
        elif self.stepCounter == 30:
            return 'UP'
        elif self.stepCounter >= 40:
            self.stepCounter = 0
            return 'RIGHT'
        else:
            return 'NONE'


game = Snake()
player = Snake_Player(game)
game.start()